import { Router } from 'express';
import signup from '../../utils/signup';

const router = Router();
router.post('/', signup);

export default router;
