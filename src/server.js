import express from 'express';
import mongoose from 'mongoose';
import sessions from 'client-sessions';
import User from './resources/signup/user.model';
import { authenticate, authorize } from './utils/abc-auth';

const app = express();
mongoose.connect('mongodb://localhost/test_db', {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
app.use(
  sessions({
    cookieName: 'session',
    secret: 'ewhfbdjefbvek&^6pi5uhegpibvwbdcjdbjd',
    duration: 1000 * 60 * 60 * 24,
    activeDuration: 1000 * 60 * 10,
    cookie: {
      httpOnly: true,
      ephemeral: false,
      secure: false
    }
  })
);
app.use(express.urlencoded({ extended: true }));

const signupFormSchema = [
  {
    property: 'firstName',
    type: 'string',
    required: true,
    alphanum: true,
    min: 3,
    max: 128
  },
  {
    property: 'lastName',
    type: 'string',
    required: true,
    alphanum: true,
    min: 3,
    max: 128
  },
  {
    property: 'email',
    type: 'email',
    required: true
  },
  {
    property: 'password',
    type: 'password',
    min: 7
  },
  {
    property: 'confirmPassword',
    type: 'password',
    min: 7
  }
];
const signinFormSchema = [
  {
    property: 'email',
    type: 'email',
    required: true
  },
  {
    property: 'password',
    type: 'password',
    min: 7
  }
];

app.use(authenticate({ signupFormSchema, signinFormSchema, User }));
app.use('/api', authorize(User));
app.get('/api/user', (req, res) => {
  res.status(200).send({ message: req.user });
});

// app.post('/signin', validate(signinFormSchema), signin(User));
export const start = () => {
  try {
    app.listen(5000, () => {
      console.log(`REST API on http://localhost:5000/`);
    });
  } catch (e) {
    console.error(e);
  }
};
