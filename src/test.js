import { Router } from 'express';
import bcrypt from 'bcryptjs';
import nJwt from 'njwt';
import { validate } from './utils/validate';

const router = Router();
const key =
  'R7/0gYz6lZi2hX0S7d+qgvZszUS9iV8kaBwbFYCcgxW68dVRmetTM4n+gjUUEfLVaDn/lRhzKEXrvj0dROi1Ne22lJ+MNB0aPXG3ATW1lb2KFmLRu61Lu73snqy31cyYGQ34/O192KsNwI7DgFdGZnH0K1cKdTOcncbosW/jMfaGzhJv/N/IqNu0GY/o9/fHLy5r1lH9URdrN64bYNxyao5Wyj59mrHrsi/FIGiPSlWGGfVFrHvbxBW6UTFBwCwRVuNPERZnLKf+GyzVwg5oYq4V5FxQE+7A179wtYRqOfnh1xuP/m2vEeVj2mmg/9T08zMSEoSFdmQs0bps3mGhqA==';

const createUserSession = (req, user) => {
  const claims = {
    scope: 'active',
    // eslint-disable-next-line no-underscore-dangle
    sub: user._id
  };
  const jwt = nJwt.create(claims, key, 'HS512');
  jwt.setExpiration(new Date().getTime + 1000 * 60 * 60 * 24);
  req.session.userToken = jwt.compact();
};

export const signup = (options) => {
  const { signupFormSchema, User } = options;
  return router.post('/', validate(signupFormSchema), async (req, res) => {
    try {
      const hash = bcrypt.hashSync(req.body.password, 14);
      req.body.password = hash;
      const user = await User.create(req.body);
      createUserSession(req, user);
      return res.status(201).send({ messagae: 'user created successfully' });
    } catch (error) {
      if (error.code === 11000) {
        return res
          .status(400)
          .send({ error: 'The email is already taken, Please try another.' });
      }
      return res.status(500).end();
    }
  });
};

export const signin = (options) => {
  const { signinFormSchema, User } = options;
  return router.post('/', validate(signinFormSchema), async (req, res) => {
    const user = await User.findOne({ email: req.body.email }).select(
      'email password'
    ).exec;
  });
};
