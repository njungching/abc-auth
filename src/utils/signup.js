import bcrypt from 'bcryptjs';
import { createUserSession } from './auth';

const sendConfirmationEmail = () => {};

const signup = (User) => {
  return async (req, res) => {
    try {
      const hash = bcrypt.hashSync(req.body.password, 14);
      req.body.password = hash;
      const user = await User.create(req.body);
      createUserSession(req, user);
      sendConfirmationEmail();
      res.status(201).send({ messagae: 'user created successfully' });
    } catch (error) {
      if (error.code === 11000) {
        res
          .status(400)
          .send({ error: 'The email is already taken, Please try another.' });
      }
      res.status(500).end();
    }
  };
};
export default signup;
