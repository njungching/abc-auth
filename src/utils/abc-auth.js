import { Router } from 'express';
import signin from './signin';
import signup from './signup';
import { validate } from './validate';
import { verifyToken } from './auth';

const router = Router();
export const authenticate = (options) => {
  const { signupFormSchema, signinFormSchema, User } = options;
  router.post('/signup', validate(signupFormSchema), signup(User));
  router.post('/signin', validate(signinFormSchema), signin(User));

  return router;
};
export const authorize = (User) => {
  return async (req, res, next) => {
    const token = req.session.userToken;
    let payload;
    try {
      payload = await verifyToken(token);
    } catch (error) {
      return res.status(401).end();
    }
    const user = await User.findById(payload.body.sub)
      .select('-password')
      .lean()
      .exec();

    if (!user) {
      return res.status(401).end();
    }
    req.user = user;
    return next();
  };
};
