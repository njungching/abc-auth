import bcrypt from 'bcryptjs';
import { createUserSession } from './auth';

const signin = (User) => {
  return async (req, res) => {
    const invalid = { error: 'Invalid email and password combination' };
    try {
      const user = await User.findOne({ email: req.body.email })
        .select('email password')
        .exec();
      if (!user || !bcrypt.compareSync(req.body.password, user.password)) {
        return res.status(401).send(invalid);
      }
      createUserSession(req, user);
      return res.status(201).send('tuma token boss');
    } catch (error) {
      console.log('An error occured', error);
      return res.status(500).end();
    }
  };
};

export default signin;
