import nJwt from 'njwt';

const key =
  'R7/0gYz6lZi2hX0S7d+qgvZszUS9iV8kaBwbFYCcgxW68dVRmetTM4n+gjUUEfLVaDn/lRhzKEXrvj0dROi1Ne22lJ+MNB0aPXG3ATW1lb2KFmLRu61Lu73snqy31cyYGQ34/O192KsNwI7DgFdGZnH0K1cKdTOcncbosW/jMfaGzhJv/N/IqNu0GY/o9/fHLy5r1lH9URdrN64bYNxyao5Wyj59mrHrsi/FIGiPSlWGGfVFrHvbxBW6UTFBwCwRVuNPERZnLKf+GyzVwg5oYq4V5FxQE+7A179wtYRqOfnh1xuP/m2vEeVj2mmg/9T08zMSEoSFdmQs0bps3mGhqA==';
const algorithm = 'HS512';

export const createToken = (user) => {
  const claims = {
    scope: 'active',
    // eslint-disable-next-line no-underscore-dangle
    sub: user._id
  };
  const jwt = nJwt.create(claims, key, algorithm);
  jwt.setExpiration(new Date().getTime() + 1000 * 60 * 60 * 24);
  return jwt.compact();
};

export const verifyToken = (token) =>
  new Promise((resolve, reject) => {
    nJwt.verify(token, key, algorithm, (err, verifiedJwt) => {
      if (err) return reject(err);
      return resolve(verifiedJwt);
    });
  });

export const createUserSession = (req, user) => {
  const token = createToken(user);
  req.session.userToken = token;
};
