import Joi from 'joi';

export const checkRequiredPorperties = (property, type, required) => {
  const ignoreCheck = ['password'];
  if (!property) {
    throw Error(
      "param missing, ensure your schema contains the 'param' property for all definitons"
    );
  }
  if (!type) {
    throw Error(
      "type missing, ensure your schema contains the 'type' property for all definitons"
    );
  }
  if (typeof required === 'undefined' && !ignoreCheck.includes(type)) {
    throw Error(
      "required missing, ensure your schema contains the 'required' property for all definitons"
    );
  }
};

const checkTypeProperty = (type) => {
  const types = ['string', 'email', 'password', 'number', 'options', 'custom'];
  if (!types.includes(type)) {
    throw Error(
      `'${type}' is not a valid type. These are the valid types [string, email, password, number, checkbox]`
    );
  }
};

const validateString = (schema) => {
  let result;
  if (schema.alphanum && schema.min && schema.max) {
    result = schema.required
      ? Joi.string()
          .alphanum()
          .min(schema.min)
          .required()
      : Joi.string()
          .alphanum()
          .min(schema.min);
  }
  if (schema.alphanum && schema.min && !schema.max) {
    result = schema.required
      ? Joi.string()
          .alphanum()
          .min(schema.min)
          .required()
      : Joi.string()
          .alphanum()
          .min(schema.min);
  }
  if (schema.alphanum && !schema.min && schema.max) {
    result = schema.required
      ? Joi.string()
          .alphanum()
          .max(schema.max)
          .required()
      : Joi.string()
          .alphanum()
          .max(schema.max);
  }
  if (!schema.alphanum && schema.min && schema.max) {
    result = schema.required
      ? Joi.string()
          .min(schema.min)
          .required()
      : Joi.string().min(schema.min);
  }
  if (!schema.alphanum && schema.min && !schema.max) {
    result = schema.required
      ? Joi.string()
          .min(schema.min)
          .required()
      : Joi.string().min(schema.min);
  }
  if (!schema.alphanum && !schema.min && !schema.max) {
    result = schema.required ? Joi.string().required() : Joi.string();
  }
  return result;
};

export const validateEmail = (schema) => {
  return schema.required
    ? Joi.string()
        .email()
        .required()
    : Joi.string().email();
};

export const validatePassword = (schema) => {
  let rule;
  if (schema.property === 'password') {
    rule = Joi.string()
      .min(schema.min)
      .required()
      .strict();
  }
  if (schema.property === 'confirmPassword') {
    rule = Joi.string()
      .valid(Joi.ref('password'))
      .required()
      .strict();
  }
  return rule;
};

export const validateOptions = (schema) => {
  const options = ['male', 'female', 'non-binary'];
  return schema.required
    ? Joi.any()
        .valid(options)
        .required()
    : Joi.any().valid(options);
};

export const applyRules = (schema, request) => {
  let rule;
  if (schema.type === 'string') {
    rule = validateString(schema);
  }
  if (schema.type === 'email') {
    rule = validateEmail(schema);
  }
  if (schema.type === 'password') {
    rule = validatePassword(schema);
  }
  if (schema.type === 'options') {
    rule = validateOptions(schema);
  }
  if (schema.custom) {
    const customJoi = Joi.extend((joi) => ({
      base: schema.required ? joi.string().required() : joi.string(),
      name: schema.type,
      language: {
        phoneNumber: schema.isValid(request.phoneNumber).message
      },
      rules: [
        {
          name: 'phoneNumber',
          validate(params, value, state, options) {
            // schema.validate(params, value, state, options, this.createError);
            if (!schema.isValid(value).valid) {
              return this.createError('string.phoneNumber', {}, state, options);
            }
            return value;
          }
        }
      ]
    }));
    rule = customJoi.string().phoneNumber();
  }
  return rule;
};

export const validate = (schema) => {
  return (req, res, next) => {
    const properties = {};
    Object.keys(schema).map((shemaObject) => {
      const { property, type, required } = schema[shemaObject];
      checkRequiredPorperties(property, type, required);
      checkTypeProperty(type);
      properties[property] = applyRules(schema[shemaObject], req.body);
      return properties;
    });

    const result = Joi.object().keys(properties);
    const { error } = Joi.validate(req.body, result);
    if (!error) {
      next();
    } else {
      const { details } = error;
      const message = details.map((i) => i.message).join(',');
      res.status(422).json({ error: message.replace(/"/g, "'") });
    }
  };
};
